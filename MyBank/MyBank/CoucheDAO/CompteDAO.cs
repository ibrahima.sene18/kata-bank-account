﻿using MyBank.Context;
using MyBank.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBank.CoucheDAO
{
    /// <summary>
    /// Cette classe permet de gerer l'acces aux Données.
    /// </summary>
    public class CompteDAO
    {
        public readonly MyBankDBContext _MyBankDBContext;
        /// <summary>
        /// Constructeur dela classe CompteDAO
        /// </summary>
        /// <param name="mybankDBContext">Le Db Context</param>
        public CompteDAO(MyBankDBContext mybankDBContext)
        {
            _MyBankDBContext = mybankDBContext;
        }

        /// <summary>
        /// Ajouter un nouveau compte
        /// </summary>
        /// <param name="compte"></param>
        public void AddCompte(Compte compte)
        {
            _MyBankDBContext.Comptes.Add(compte);
            _MyBankDBContext.SaveChanges();
        }

        /// <summary>
        /// Ajouter des transaction c'est à dire faire un dépot(Credit) ou un retrait(Debit).
        /// </summary>
        /// <param name="id">Numero de compte du client</param>
        /// <param name="transaction"></param>
        public void AddTransaction(int id, Transaction transaction)
        {
            var item = _MyBankDBContext.Comptes.FirstOrDefault(x => x.CompteId == id);
            if (item != null)
            {
                if (transaction.Operation == "Credit")
                {
                    if (transaction.Montant < 0)
                    {
                        transaction.Montant *= -1;
                    }
                }
                if (transaction.Operation == "Debit")
                {
                    if (transaction.Montant > 0)
                    {
                        transaction.Montant *= -1;
                    }
                }
                transaction.Solde = GetSoldeAfter(id, transaction.Montant);
                _MyBankDBContext.Transactions.Add(transaction);
                _MyBankDBContext.SaveChanges();
            }
        }

        /// <summary>
        /// Cette methode permet d'avoir la valeur du solde du compte apres la transaction.
        /// </summary>
        /// <param name="id">Numéro de compte du client</param>
        /// <param name="montantTransact">Montant de la transaction</param>
        /// <returns></returns>
        public double GetSoldeAfter(int id, double montantTransact)
        {
            var balance = from transaction in _MyBankDBContext.Transactions where transaction.CompteId == id select transaction;
            double solde = 0;
            foreach (var item in balance)
            {
                solde += item.Montant;
            }
            solde += montantTransact;
            return solde;
        }

        /// <summary>
        /// Retourne la liste des transactions d'un client.
        /// </summary>
        /// <param name="id">Numero de compte du client</param>
        /// <returns>IEnumerable<Transaction></returns>
        public IEnumerable<Transaction> GetHistoriqueById(int id)
        {
            var historique = from transaction in _MyBankDBContext.Transactions where transaction.CompteId == id orderby transaction.DateTransaction descending select transaction;
            return historique;
        }

        /// <summary>
        /// Retourne le Relevé des transactions d'un client.
        /// </summary>
        /// <param name="id">Numero de compte du client</param>
        /// <returns>List<(DateTime, double, double)></returns>
        public List<(DateTime, double, double)> GetReleveById(int id)
        {
            var releve = from transaction in _MyBankDBContext.Transactions where transaction.CompteId == id orderby transaction.DateTransaction descending select transaction;
            List<(DateTime dateTrans, double montant, double solde)> lstReleve = new List<(DateTime, double, double)>();
            foreach (var item in releve)
            {
                lstReleve.Add((item.DateTransaction, item.Montant, item.Solde));
            }
            return lstReleve;
        }

        /// <summary>
        /// Cette methodes permet d'imprimer le releve à partir de l'Id
        /// </summary>
        /// <param name="id">Numero du client</param>
        public string PrintReleveById(int id)
        {
            var releve = from transaction in _MyBankDBContext.Transactions where transaction.CompteId == id orderby transaction.DateTransaction descending select transaction;
            string releveString = "Date\t\t\tMontant\t\tSolde\n\n";
            foreach (var item in releve)
            {
                releveString += string.Format("{0:d} à {0:t}\t{1}\t\t{2}\n",item.DateTransaction, item.Montant.ToString(), item.Solde.ToString());
            }
            return releveString;
        }
    }
}