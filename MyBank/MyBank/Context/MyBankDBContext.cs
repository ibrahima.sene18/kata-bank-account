﻿using Microsoft.EntityFrameworkCore;
using MyBank.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyBank.Context
{
    /// <summary>
    /// La classe MyBankDBContext herite de la classe DbContext.
    /// </summary>
    public class MyBankDBContext : DbContext
    {
        /// <summary>
        /// Le constructeur de la classe MyBankDBContext
        /// </summary>
        /// <param name="options"></param>
        public MyBankDBContext(DbContextOptions<MyBankDBContext> options) : base(options)
        {

        }

        public DbSet<Compte> Comptes { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
    }
}
