﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyBank.Models
{
    /// <summary>
    /// Classe Compte
    /// </summary>
    public class Compte
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CompteId { get; set; }

        [Required]
        [StringLength(100)]
        public string Nom { get; set; }

        [Required]
        [StringLength(100)]
        public string Prenom { get; set; }

        [Required]
        public DateTime DateNaissance { get; set; }

        /// <summary>
        /// Le contructeur par defaut de la classe Compte
        /// </summary>
        public Compte()
        {

        }

        /// <summary>
        /// /Constructeur de la classe Compte
        /// </summary>
        /// <param name="nom">Nom du client</param>
        /// <param name="prenom">Prenom </param>
        /// <param name="dateCreation"></param>
        public Compte(string nom, string prenom, DateTime dateCreation)
        {
            Compte compte = new Compte();
            Nom = nom;
            Prenom = prenom;
            DateNaissance = dateCreation;
        }
    }
}
