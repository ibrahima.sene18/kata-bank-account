﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyBank.Models
{
    /// <summary>
    /// Classe Transaction qui traite les transactions des comptes bancaires
    /// </summary>
    public class Transaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TransactionId { get; set; }

        [Required]
        [StringLength(50)]
        public string Operation { get; set; }

        [Required]
        [StringLength(150)]
        public string Libelle { get; set; }

        [Required]
        public DateTime DateTransaction { get; set; }

        [Required]
        public double Montant { get; set; }

        public double Solde { get; set; }

        public int CompteId { get; set; }

        [ForeignKey("CompteId")]
        public virtual Compte Comptes { get; set; }

        /// <summary>
        /// Constructeur san parametre
        /// </summary>
        public Transaction()
        {

        }

        /// <summary>
        /// Constructeur avec 6 parametres
        /// </summary>
        /// <param name="operation">Ce paramèmtre donne l'information sur le type de la transaction. Il ne prend que les "Credit" ou "Debit".</param>
        /// <param name="libelle">Ce parametre permet de donner les details de la transactions.</param>
        /// <param name="dateTransaction">Ce parametre permet de donner la date de la transaction.</param>
        /// <param name="montant">Ce parametre permet de donner le montant de la transaction.</param>
        /// <param name="solde">Ce parametre permet de donner le solde du compte apres la transaction.</param>
        /// <param name="compteId">Ce parametre permet de donner l'Id du Compte du client qui veut faire une transaction.</param>
        public Transaction(string operation, string libelle, DateTime dateTransaction, double montant, double solde, int compteId)
        {
            Transaction transaction = new Transaction();
            Operation = operation;
            Libelle = libelle;
            DateTransaction = dateTransaction;
            Montant = montant;
            Solde = solde;
            CompteId = compteId;
        }
    }
}