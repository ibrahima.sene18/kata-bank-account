﻿using Microsoft.AspNetCore.Mvc;
using MyBank.Context;
using MyBank.CoucheDAO;
using MyBank.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MyBank.Controllers
{
    /// <summary>
    /// C'est le controleur de notre banque nommé CompteControleur
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CompteController : ControllerBase
    {
        public readonly MyBankDBContext _MyBankDBContext;
        public readonly CompteDAO compteDAO;

        /// <summary>
        /// Le Constructeur du controleur CompteController
        /// </summary>
        /// <param name="myBankDBContext">C'est un parametre de type BanqueDBContext</param>
        public CompteController(MyBankDBContext myBankDBContext)
        {
            _MyBankDBContext = myBankDBContext;
            compteDAO = new CompteDAO(_MyBankDBContext);
        }

        /// <summary>
        /// Ajouter un nouveau compte
        /// </summary>
        /// <param name="compte"></param>
        // POST api/Compte/addCompte
        [HttpPost("/api/Compte/addCompte")]
        public void AddCompte([FromBody] Compte compte)
        {
            compteDAO.AddCompte(compte);
        }

        /// <summary>
        /// Ajouter des transaction c'est à dire faire de dépot(Credit) ou un retrait(Debit).
        /// </summary>
        /// <param name="id">Numero de compte du client</param>
        /// <param name="transaction"></param>
        // POST api/Compte/addTransaction
        [HttpPost("/api/Compte/addTransaction")]
        public void AddTransaction(int id, [FromBody] Transaction transaction)
        {
            if (id == transaction.CompteId)
            {
                compteDAO.AddTransaction(id, transaction);
            }
        }

        /// <summary>
        /// Retourne la liste des transactions d'un client.
        /// </summary>
        /// <param name="id">Numero de compte du client</param>
        /// <returns>IEnumerable<Transaction></returns>
        // GET api/getHistoriqueByCompteId/{id}/1
        [HttpGet("/api/Compte/getHistoriqueByCompteId/{id}")]
        public IEnumerable<Transaction> GetHistoriqueByCompteId(int id)
        {
            return compteDAO.GetHistoriqueById(id);
        }

        /// <summary>
        /// Retourne le Relevé des transactions d'un client.
        /// </summary>
        /// <param name="id">Numero de compte du client</param>
        /// <returns>List<(DateTime, double, double)></returns>
        // GET api/getReleveId/1
        [HttpGet("/api/Compte/getReleveById/{id}")]
        //public List<(DateTime, double, double)> GetReleveByCompteId(int id)
        public List<(DateTime, double, double)> GetReleveById(int id)
        {
            return compteDAO.GetReleveById(id);
        }

        /// <summary>
        /// Imprime la liste des transactions d'un client.
        /// </summary>
        /// <param name="id">Numero de compte du client</param>
        // GET api/printReleveById/{id}/1
        [HttpGet("/api/Compte/PrintReleveById/{id}")]
        public string PrintReleveById(int id)
        {
            return compteDAO.PrintReleveById(id);
        }
    }
}
